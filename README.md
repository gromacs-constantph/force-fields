# force fields

## CHARMM36*

CHARMM36* force field (in the folder ``charmm36-mar2019-cphmd.ff``) includes modifications to the dihedral parameters of some atoms in the titratable amino acid sidechains, to better converge the rotameric configurations. 
Atoms whose parameters have been modified, are denoted with 'n', such as CT2n and OH1n. The force field also includes parameters for the buffer particle (BUF). 

## MARTINI

For Martini 2.2, no modification were made. For Martini, the force field folder ``martini`` contains the input files for an example system (aspartic acid), for which the buffer topology is also given in ``topol.top`` .

